package com.emran.mvparchitecture.model;

import com.emran.mvparchitecture.presenter.LoginPresenter;
import com.emran.mvparchitecture.view.LoginView;

/**
 * Created by emran on 2/10/18.
 */

public class PresenterLogin implements LoginPresenter {

    private LoginView loginView;

    public PresenterLogin(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void performLogin(String userName, String password) {

        if (userName == null || password == null || password.isEmpty() || userName.isEmpty()) {
            this.loginView.loginValidation();
        } else {
            if (userName.equalsIgnoreCase("Emran") && password.equalsIgnoreCase("123456")) {
                this.loginView.loginSuccess();
            } else {
                this.loginView.loginError();
            }
        }
    }
}
