package com.emran.mvparchitecture.view;

/**
 * Created by emran on 2/10/18.
 */

public interface LoginView {

    void loginValidation();
    void loginSuccess();
    void loginError();
}
