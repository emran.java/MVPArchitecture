package com.emran.mvparchitecture.activitys;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.emran.mvparchitecture.R;
import com.emran.mvparchitecture.model.PresenterLogin;
import com.emran.mvparchitecture.presenter.LoginPresenter;
import com.emran.mvparchitecture.view.LoginView;

public class MainActivity extends AppCompatActivity {

    private Context mContext;

    private EditText edtId, edtPass;

    private Button buttonLogin;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        edtId = (EditText) findViewById(R.id.edtId);
        edtPass = (EditText) findViewById(R.id.edtPass);

        buttonLogin = (Button) findViewById(R.id.buttonLogin);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setValues();
            }
        });

        loginPresenter = new PresenterLogin(new LoginView() {
            @Override
            public void loginValidation() {
                Toast.makeText(mContext, "Please give valide user name or password", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void loginSuccess() {
                Toast.makeText(mContext, "Login success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void loginError() {
                Toast.makeText(mContext, "Login un-success", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setValues() {
        String id = edtId.getText().toString();
        String passwd = edtPass.getText().toString();

        loginPresenter.performLogin(id, passwd);

    }
}
