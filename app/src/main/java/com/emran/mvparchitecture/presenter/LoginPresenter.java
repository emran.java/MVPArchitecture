package com.emran.mvparchitecture.presenter;

/**
 * Created by emran on 2/10/18.
 */

public interface LoginPresenter {
    void performLogin(String userName, String password);
}
